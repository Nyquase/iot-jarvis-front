let mqtt = require('mqtt');
let faker = require('faker');
let client = mqtt.connect('mqtt://172.20.10.10');

client.on('connect', function () {
  client.subscribe('presence', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt')
    }
  })
});

client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString());
});

setInterval(() => {
  client.publish('patients/Renaud/heartrate', faker.random.number({min:70, max:80}).toString());
}, 1000);
