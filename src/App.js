import React, {useEffect, useState} from 'react';
import mqtt from 'mqtt'
import PatientsView from './components/PatientsView';
import './App.css';

function App() {
  let p = {
    test: {heartrate: 50, data: []},
  };
  let [patients, setPatients] = useState(p);
  useEffect(() => {
    let client = mqtt.connect('mqtt://172.20.10.10:9001');
    // let client = mqtt.connect('mqtt://localhost:9001');
    client.on('connect', function () {
      client.subscribe('patients/+/heartrate');
    });

    client.on('message', function (topic, message) {
      console.log(topic, message.toString());
      let t = topic.split('/');
      if (t[0] === "patients") {
        let [_, patientName, _dataType] = t;
        let data = [];
        if (patients[patientName]) {
          data = patients[patientName].data.concat(Number(message));
        } else {
          data = [Number(message)];
        }
        setPatients({
          ...patients,
          [patientName]: {
            heartrate: Number(message),
            data: data,
          }
        });
      }
    });
    return () => {
      client.end();
    }
  });

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-title">
          Jarvis
        </h1>
      </header>
      <PatientsView patients={patients} />
    </div>
  );
}

export default App;
