import React, { useState, useEffect } from 'react';
import Chart from 'react-apexcharts';
import faker from 'faker';
import { Container, Col, Row, Button } from 'react-bootstrap';

function PatientsView(props) {
    const options = {
        chart: {
            type: 'line',
            toolbar: {
                show: false
            },
            animations: {
                enabled: true,
                easing: 'linear',
                dynamicAnimation: {
                    speed: 1000
                }
            },
        },
        xaxis: {
            type: "numeric",
            range: 20
        },
        yaxis: {
            min: 50,
            max: 180
        }
    };

    return (
        <Container fluid={true} >
            <Row style={{ justifyContent: "space-evenly" }}>
                {
                    Object.entries(props.patients).map(([key, patient]) => (
                        <Col key={key} className="" style={{ position: "relative", border: "solid black 2px" }} sm={3}>
                            <Button className="border-top-0 border-right-0 rounded-0" variant="outline-primary" style={{ position: "absolute", right: -1, top: -1, padding: "4px 12px", borderBottomLeftRadius: "5px" }} >
                                <i className="icon ion-md-expand" style={{ fontSize: "1.5em" }} />
                            </Button>
                            <h3>
                                {key}
                            </h3>
                            <i>
                                Heart Rate: {patient.heartrate}
                            </i>
                            <Chart options={options} series={[{name: 'Heart Rate', data: patient.data}]} type="line" height="350" />

                        </Col>
                    ))
                }
            </Row>
        </Container >
    );
}

export default PatientsView;
